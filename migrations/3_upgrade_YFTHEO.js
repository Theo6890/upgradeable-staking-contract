const { upgradeProxy } = require("@openzeppelin/truffle-upgrades");

const YFTHEO = artifacts.require("YFTHEO");
const YFTHEOv = artifacts.require("YFTHEOv");

module.exports = async function (deployer, network, accounts) {
  const instance = await YFTHEO.deployed().then(async (yf) => {
    return upgradeProxy(yf.address, YFTHEOv, { deployer });
  });

  console.log("Upgraded", instance.address);
};

const { upgradeProxy } = require("@openzeppelin/truffle-upgrades");

const YFTHEOv = artifacts.require("YFTHEOv");
const YFTHEOvFakeTime = artifacts.require("YFTHEOvFakeTime");

module.exports = async function (deployer, network, accounts) {
  const instance = await YFTHEOv.deployed().then(async (yf) => {
    return upgradeProxy(yf.address, YFTHEOvFakeTime, { deployer });
  });

  console.log("Upgraded", instance.address);
};

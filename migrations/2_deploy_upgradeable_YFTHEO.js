const { deployProxy } = require("@openzeppelin/truffle-upgrades");

const YFTHEO = artifacts.require("YFTHEO");

module.exports = async function (deployer, network, accounts) {
  console.log("deployer", deployer.networks.from);

  const instance = await deployProxy(YFTHEO, { deployer });
  console.log("Deployed", instance.address);
};

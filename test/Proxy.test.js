// Created contracts
const YFTHEO = artifacts.require("YFTHEO");
const YFTHEOv = artifacts.require("YFTHEOv");
const YFTHEOvFakeTime = artifacts.require("YFTHEOvFakeTime");
//Module loading
require("chai").use(require("chai-as-promised")).should();
const truffleAssert = require("truffle-assertions");
const Utils = require("./Utils.js");
const { deployProxy, upgradeProxy } = require("@openzeppelin/truffle-upgrades");
let proxy;

contract("Proxy", (accounts) => {
  describe("Proxy interactions", async () => {
    it("works before and after upgrading", async function () {
      proxy = await deployProxy(YFTHEO);
      await upgradeProxy(proxy.address, YFTHEOv);
      proxy = await upgradeProxy(proxy.address, YFTHEOvFakeTime); // why need to refresh proxy?
    });

    it("proxy call", async () => {
      let res = await proxy.fakeTimer({ from: accounts[2] });
      res = parseFloat(res);
      console.log("res: ", res);
      expect(res).to.be.gte(Math.pow(10, 9)); // equals zero because execute directly no pause
    });

    it("uses Kovan proxy for YFTHEOvFakeTime", async () => {
      // Usable due to kovan fork with ganache
      const kovanProxAddr = "0x09100AC1bB44B4CF5b94Cd40A83d9364BE7B4476";
      const prox = await YFTHEOvFakeTime.at(kovanProxAddr);
      let res = await prox.fakeTimer({ from: accounts[2] });
      expect(parseFloat(res)).to.be.gte(Math.pow(10, 9)); // equals zero because execute directly no pause
    });
  });
});

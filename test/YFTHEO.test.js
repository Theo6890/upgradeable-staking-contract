// Created contracts
const ForceSend = artifacts.require("ForceSend");
const DAI = artifacts.require("DAI");
const YFTHEO = artifacts.require("YFTHEO");
//Module loading
require("chai").use(require("chai-as-promised")).should();
const truffleAssert = require("truffle-assertions");
const Utils = require("./Utils.js");

contract("YFTHEO", (accounts) => {
  let dai;
  const DAIaddress = "0x6B175474E89094C44Da98b954EedeAC495271d0F",
    binance8 = "0xF977814e90dA44bFA03b6295A0616a897441aceC";

  before(async () => {
    // Load Contracts
    yfTHEO = await YFTHEO.new();
    // Instanciate DAI
    dai = await DAI.at(DAIaddress);
  });

  describe("DAI minting", async () => {
    it("should send ether to the Dai contract", async () => {
      // Send 1 eth to daiAddress to have gas to mint.
      // Uses ForceSend contract, otherwise just sending
      // a normal tx will revert.
      const forceSend = await ForceSend.new();
      await forceSend.go(dai.address, { value: Utils.toWei("1") });
      let ethBalance = await Utils.getETHBalance(dai.address);
      ethBalance = parseFloat(Utils.fromWei(ethBalance));
      expect(ethBalance).to.be.gte(1);
    });

    it("transfers 10k DAI to accounts[0]", async () => {
      await truffleAssert.passes(
        dai.transfer(accounts[0], Utils.toWei(10000), { from: binance8 })
      );

      let DAIBalance = await dai.balanceOf(accounts[0]);
      DAIBalance = parseFloat(Utils.fromWei(DAIBalance));
      expect(DAIBalance).to.be.gte(1);
    });

    it("transfers 10k YFTHEO", async () => {
      await truffleAssert.passes(
        dai.transfer(yfTHEO.address, Utils.toWei(10000), { from: binance8 })
      );

      let DAIBalance = await dai.balanceOf(accounts[0]);
      DAIBalance = parseFloat(Utils.fromWei(DAIBalance));
      expect(DAIBalance).to.be.gte(1);
    });
  });

  describe("YFTHEO interactions", async () => {
    it("stakes 1k $DAI", async () => {
      await dai.approve(yfTHEO.address, Utils.toWei(1000));

      const res = await yfTHEO.stake(Utils.toWei(1000));
      truffleAssert.eventEmitted(res, "TokenStaked", (result) => {
        const staked = parseFloat(Utils.fromWei(result.staked));
        console.log("staked amount: ", staked);
        return expect(staked).to.be.equal(1000);
      });
    });

    it("gets pending rewards", async () => {
      let res = await yfTHEO.getEarningAmount(accounts[0]);
      res = parseFloat(Utils.fromWei(res));
      // expect(res).to.be.gte(0.00030); // equals zero because execute directly no pause
    });

    it("claims pending rewards", async () => {
      let res = await yfTHEO.claim(accounts[0]);
      truffleAssert.eventEmitted(res, "RewardsClaimed", (result) => {
        const claimingAmount = parseFloat(Utils.fromWei(result.claimed));
        console.log("claimed amount: ", claimingAmount);
        return expect(claimingAmount).to.be.gte(0.0003);
      });
    });

    it("unstakes tokens", async () => {
      let res = await yfTHEO.unstake(accounts[0]);
      truffleAssert.eventEmitted(res, "TokenUnstaked", (result) => {
        const unstaked = parseFloat(Utils.fromWei(result.unstaked));
        console.log("unstaked amount: ", unstaked);
        return expect(unstaked).to.be.gte(1000);
      });
    });
  });
});

## What is a proxy?

A proxy is a contract that delegates all of its calls to a second contract, named an implementation contract. A proxy can be upgraded by its admin to use a different implementation contract.
**All state and funds are held in the proxy.**

## Migration to upgrade proxy

In fact all files are needed: the one that has deployed the base contract & the new one.
upgradeProxy() manage on its own which migration to use for update.

## Deployed

- (YFTHEO)[https://kovan.etherscan.io/address/0xe17f63499263994cc2ac3de1470e3d0b1e4675d4]
- (YFTHEOv - deployed 5 minutes later)[https://kovan.etherscan.io/address/0xfBc67C067e4774dBe70E12dae1Be8529BB5f5411]
- (YFTHEOvFakeTime)[https://kovan.etherscan.io/address/0x7af65a85e43392bf7fcf52dee763df8f4feb9661]

## Upgrade

- will surely have to run `truffle compile -all` before migrating to newer version. This is due to truffle AST more [here](https://docs.openzeppelin.com/upgrades-plugins/1.x/faq#why-do-i-have-to-recompile-all-contracts-for-truffle)
- Always returns (AdminUpgradeabilityProxy address)[https://kovan.etherscan.io/address/0x09100AC1bB44B4CF5b94Cd40A83d9364BE7B4476]
- One migration file per new version and keep previous ones otherwise it cannnot upgrade

## Using proxy in the front or in tests

- Latest implementation ABI contract MUST be use in the front and test AT **proxy.address**

```
const v1 = await V1.at(proxy.address)
v1.oldMethod()
const v2 = await V2.at(proxy.address)
v2.newMethod()
```

## Ledger: derivation path

Using Ledger path: `m/44'/60'/4'/0/0` where `4` is the index of the wanted address. Check [this](https://github.com/ethereum/EIPs/issues/84) for more info.

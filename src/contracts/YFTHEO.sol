// SPDX-License-Identifier: MIT
pragma solidity ^0.8.1;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract YFTHEO {
    ERC20 public constant DAI =
        ERC20(0x6B175474E89094C44Da98b954EedeAC495271d0F);
    uint256 public constant APY = 950; // 950%
    uint256 public constant YEAR_BASED = 365 days;

    mapping(address => uint256) public amountStaked;
    mapping(address => uint256) public lastClaimAt;
    mapping(address => uint256) public claimedRewards;

    event TokenStaked(address owner, uint256 staked, uint256 stakedAt);
    event TokenUnstaked(address owner, uint256 unstaked, uint256 unstakedAt);
    event RewardsClaimed(address owner, uint256 claimed, uint256 claimedAt);

    function stake(uint256 _amount) public {
        claim(msg.sender);

        DAI.transferFrom(msg.sender, address(this), _amount);

        amountStaked[msg.sender] += _amount;
        lastClaimAt[msg.sender] = block.timestamp;

        emit TokenStaked(msg.sender, _amount, lastClaimAt[msg.sender]);
    }

    function getEarningAmount(address _owner)
        public
        view
        returns (uint256 pending)
    {
        uint256 staked = amountStaked[_owner];
        uint256 timeDiff = block.timestamp - lastClaimAt[_owner];

        pending = (staked * APY * timeDiff) / YEAR_BASED / 100;
    }

    function isStaking(address _owner) public view returns (bool) {
        return amountStaked[_owner] > 0;
    }

    function claim(address _owner) public {
        uint256 claimingAmount = getEarningAmount(_owner);

        DAI.approve(address(this), claimingAmount);
        DAI.transfer(_owner, claimingAmount);

        lastClaimAt[_owner] = block.timestamp;
        claimedRewards[_owner] += claimingAmount;

        emit RewardsClaimed(_owner, claimingAmount, lastClaimAt[_owner]);
    }

    // Pending tokens & staked tokens
    function unstake(address _owner) public {
        uint256 earnings = getEarningAmount(_owner);
        uint256 totalAmount = earnings + amountStaked[_owner];

        DAI.approve(address(this), totalAmount);
        DAI.transfer(_owner, totalAmount);

        lastClaimAt[_owner] = block.timestamp;
        claimedRewards[_owner] += earnings;

        emit TokenUnstaked(_owner, totalAmount, lastClaimAt[_owner]);
    }
}

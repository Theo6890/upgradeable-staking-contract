// SPDX-License-Identifier: MIT
pragma solidity ^0.8.1;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract YFTHEOvFakeTime {
    ERC20 public constant DAI =
        ERC20(0x6B175474E89094C44Da98b954EedeAC495271d0F);
    uint256 public constant APY = 950; // 950%
    uint256 public constant YEAR_BASED = 365 days;

    mapping(address => uint256) public amountStaked;
    mapping(address => uint256) public lastClaimAt;
    mapping(address => uint256) public claimedRewards;

    event TokenStaked(address owner, uint256 staked, uint256 stakedAt);
    event TokenUnstaked(address owner, uint256 unstaked, uint256 unstakedAt);
    event RewardsClaimed(address owner, uint256 claimed, uint256 claimedAt);

    function stake(uint256 _amount) public {
        DAI.transferFrom(msg.sender, address(this), _amount);

        amountStaked[msg.sender] += _amount;
        lastClaimAt[msg.sender] = block.timestamp;

        emit TokenStaked(msg.sender, _amount, lastClaimAt[msg.sender]);
    }

    function isStaking(address _owner) public view returns (bool) {
        return amountStaked[_owner] > 0;
    }

    function fakeTimer() public view returns (uint256) {
        return block.timestamp + 10352;
    }
}
